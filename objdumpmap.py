#!/usr/bin/env python
import sys
import subprocess
import re
import os.path

def main():
    if len(sys.argv) < 2:
        print('Missing argument')
        sys.exit(-1)

    exe = sys.argv[1]
    
    if not os.path.isfile(exe):
        print('Not a file, sir.')
        exit(-2)

    o = subprocess.check_output(['riscv64-unknown-elf-objdump', '-d', '-g', exe])
    s = subprocess.check_output(['strings', '-t', 'x', exe])

    # match addresses in strings output
    regex = re.compile('^[ ]*([0-9a-f]+) (.*)$')

    strings = {}
    for line in str(s).split('\n'):
        match = regex.search(line)
        if not match:
            continue

        (addr, string) = match.groups()
        strings[int(addr, 16)+0x10000] = string
    
    # Match addresses and strings
    def stringrepl(matchobj):
        # if matchobj is None:
        #     return None
        saddr = matchobj.groups()[1]
        addr = int(saddr, 16)
        if addr in strings:
            return '%s ;; "%s"' % (matchobj.groups()[0], strings[addr])

        return matchobj.groups()[0]

    replaced = re.sub(r'(.*\# ([0-9a-f]+).*errno.*)', stringrepl, str(o))

    print(replaced)

if __name__ == '__main__':
    main()

